﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace OperationsService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        [WebGet(UriTemplate = "/suma/?numero1={numero1}&numero2={numero2}",
            ResponseFormat = WebMessageFormat.Json)]
        Resultado GetSumaTotal(double numero1, double numero2);

        [OperationContract]
        [WebGet(UriTemplate = "/resta/?numero1={numero1}&numero2={numero2}",
           ResponseFormat = WebMessageFormat.Json)]
        Resultado GetRestaTotal(double numero1, double numero2);

        [OperationContract]
        [WebGet(UriTemplate = "/multiple/?numero1={numero1}&numero2={numero2}&tipo={tipo}",
            ResponseFormat = WebMessageFormat.Json)]
        ResultadoMultiple GetMultipleTotal(double numero1, double numero2, bool tipo);
    }

    [DataContract]
    public class Resultado
    {
        [DataMember]
        public double total;
    }

    [DataContract]
    public class ResultadoMultiple
    {
        [DataMember]
        public double total1;
        [DataMember]
        public double total2;
    }
}
