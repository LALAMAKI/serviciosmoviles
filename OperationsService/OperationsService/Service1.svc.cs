﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace OperationsService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public Resultado GetSumaTotal(double numero1, double numero2)
        {
            Resultado resultado = new Resultado();

            resultado.total = numero1 + numero2;

            return resultado;
        }

        public Resultado GetRestaTotal(double numero1, double numero2)
        {
            Resultado resultado = new Resultado();

            resultado.total = numero1 - numero2;

            return resultado;
        }

        public ResultadoMultiple GetMultipleTotal(double numero1, double numero2, bool tipo)
        {
            ResultadoMultiple resultadoMultiple = new ResultadoMultiple();

            if (tipo)
            {
                resultadoMultiple.total1 = numero1 + numero2;
                resultadoMultiple.total2 = numero1 - numero2;
            }
            else
            {
                resultadoMultiple.total1 = numero1 * numero2;
                resultadoMultiple.total2 = numero1 / numero2;
            }

            return resultadoMultiple;
        }


    }
}
