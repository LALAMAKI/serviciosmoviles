﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MisDirecciones
{
    class Program
    {
        static void Main(string[] args)
        {
            string olat = "32.626312";
            string olong = "-115.377047";

            string dlat= "32.630732";
            string dlong = "-115.420297";
            string direccion = "Fernando montes de oca 847";
            string direccion1 = "rio presidio";

            Uri uri = new Uri(@"https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="+ direccion+ "&destinations=" + direccion1+ "&key=AIzaSyAAqdQzKGEVMOH4Yn4P4mlzq9qHvsWWbr0");

            WebRequest webRequest = WebRequest.Create(uri);
            WebResponse response = webRequest.GetResponse();
            StreamReader streamReader = new StreamReader(response.GetResponseStream());
            string respuesta = streamReader.ReadToEnd();
            Console.WriteLine(respuesta);

            var jsonobject = JsonConvert.DeserializeObject<RootObject>(respuesta);

            string daddress = jsonobject.destination_addresses[0];
            string oaddress = jsonobject.origin_addresses[0];

            Console.WriteLine("La direccion destino:" + daddress);
            Console.WriteLine("La direccion origen:" + oaddress);

            Console.ReadKey();
        }
    }

    public class Distance
    {
        public string text { get; set; }
        public int value { get; set; }
    }

    public class Duration
    {
        public string text { get; set; }
        public int value { get; set; }
    }

    public class Element
    {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }
    }

    public class Row
    {
        public List<Element> elements { get; set; }
    }

    public class RootObject
    {
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public List<Row> rows { get; set; }
        public string status { get; set; }
    }
}
