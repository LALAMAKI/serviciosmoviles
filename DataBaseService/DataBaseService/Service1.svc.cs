﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DataBaseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
     
        public Alumnos GetAlumnos()
        {
            Alumnos alumnos = new Alumnos();
            alumnos.alumnoslista = new List<Alumno>();

            string query = "Select A.nombre, a.apellidop, a.apellidom, a.NSS, a.RFC, a.matricula, c.nombre as carrera " +
                "from ALUMNOS AS A, CARRERA AS C " +
                "WHERE c.idcarrera = a.idcarrera";

            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = "Server=DESKTOP-D46BEAG\\SQLEXPRESS;Database=escuela;Integrated Security=True";
            sqlConnection.Open();

            DataSet ds = new DataSet();
            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            adapter.Fill(ds);

            sqlConnection.Close();

            for (int i=0;i< ds.Tables[0].Rows.Count; i++)
            {
                Alumno alumno = new Alumno();
                alumno.nombre = ds.Tables[0].Rows[i]["nombre"].ToString();
                alumno.apellidop = ds.Tables[0].Rows[i]["apellidop"].ToString();
                alumno.apellidom = ds.Tables[0].Rows[i]["apellidom"].ToString();
                alumno.NSS = ds.Tables[0].Rows[i]["NSS"].ToString();
                alumno.RFC = ds.Tables[0].Rows[i]["RFC"].ToString();
                alumno.carrera = ds.Tables[0].Rows[i]["carrera"].ToString();
                alumno.matricula = Convert.ToInt32(ds.Tables[0].Rows[i]["matricula"]);
                alumnos.alumnoslista.Add(alumno);
            }
            return alumnos;
        }
    }
}
